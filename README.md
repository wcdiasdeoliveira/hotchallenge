# HotChallenge

## Synopsis

Hello!

This is just a demo app, only to comprove some skills. 

It's a simple project containning two artefacts: HotChallengeWeb and HotChallengeFileGateway.
The first it's a simple web application to list, download and save some files via an second API. 
The other one it's to process that requests in a REST way. 

It was built using some reference of the Spring Documentation and you can notice some similarity in the store package. 

In special, the base doc was:

https://spring.io/guides/gs/uploading-files/

I did not pretend to develop all validations and features that that kind of app require but it seems to run really good 
and has a minimum test coverage.

## Installation

Clone the repository:

git clone https://github.com/wcdoliveira/HotChallenge

Inside the respective folder, run:

./gradlew build && java -jar build/libs/<APP_TO_RUN>.jar

## Contributors

Wesley C. Dias de Oliveira

## License

Free for use and extend.

