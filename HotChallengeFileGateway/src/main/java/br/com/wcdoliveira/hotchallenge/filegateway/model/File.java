package br.com.wcdoliveira.hotchallenge.filegateway.model;

import java.util.Date;

public class File {
	
	private Long userId;	
	private String name;
	private Status status;
	private Date uploadedAt;
	private Integer blockDividers;
	private String publicLinkToDownload;
	
	public File(Long userId, String name, Status status, Date uploadedAt, Integer blockDividers, String publicLinkToDownload){
		this.setUserId(userId);
		this.setName(name);
		this.setStatus(status);
		this.setUploadedAt(uploadedAt);
		this.setBlockDividers(blockDividers);
		this.setPublicLinkToDownload(publicLinkToDownload);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getUploadedAt() {
		return uploadedAt;
	}

	public void setUploadedAt(Date uploadedAt) {
		this.uploadedAt = uploadedAt;
	}

	public Integer getBlockDividers() {
		return blockDividers;
	}

	public void setBlockDividers(Integer blockDividers) {
		this.blockDividers = blockDividers;
	}

	public String getPublicLinkToDownload() {
		return publicLinkToDownload;
	}

	public void setPublicLinkToDownload(String publicLinkToDownload) {
		this.publicLinkToDownload = publicLinkToDownload;
	}	
}