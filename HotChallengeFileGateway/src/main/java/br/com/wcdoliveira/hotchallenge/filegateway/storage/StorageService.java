package br.com.wcdoliveira.hotchallenge.filegateway.storage;

import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import br.com.wcdoliveira.hotchallenge.filegateway.model.File;

public interface StorageService {

    void init();

    File store(MultipartFile file, Long userId, int chunk, int chunks);

    Path load(String filename);

    Resource loadAsResource(String filename);
    
    void deleteAll();
}