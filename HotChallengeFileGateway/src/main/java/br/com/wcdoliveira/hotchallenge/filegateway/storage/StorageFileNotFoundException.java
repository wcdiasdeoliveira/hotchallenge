package br.com.wcdoliveira.hotchallenge.filegateway.storage;

public class StorageFileNotFoundException extends StorageException {

	private static final long serialVersionUID = -3624465942742775658L;

	public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}