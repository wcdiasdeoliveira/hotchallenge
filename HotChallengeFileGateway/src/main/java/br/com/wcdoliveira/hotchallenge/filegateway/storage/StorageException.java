package br.com.wcdoliveira.hotchallenge.filegateway.storage;

public class StorageException extends RuntimeException {

	private static final long serialVersionUID = -7389249942375132115L;

	public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
