
package br.com.wcdoliveira.hotchallenge.filegateway.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.wcdoliveira.hotchallenge.filegateway.model.File;
import br.com.wcdoliveira.hotchallenge.filegateway.model.Status;
import br.com.wcdoliveira.hotchallenge.filegateway.storage.StorageService;

@RestController
public class FileController {  
	
    private final StorageService storageService;
    
    private ArrayList<File> files;
    
    @Autowired
    public FileController(StorageService storageService) {
        this.storageService = storageService;
        this.files = new ArrayList<>();
    }
	
	@CrossOrigin
	@RequestMapping(value="/index",method=RequestMethod.GET)
    public ArrayList<File> index() {	
        return this.files;        
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/upload-dir/{filename:.+}",method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
		try {
	        Resource file = storageService.loadAsResource(filename);
	        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
	                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
		} catch (Exception ignored) {
			return new ResponseEntity( new File(0L, "", Status.ERROR, new Date(), 0,""),HttpStatus.OK);					
		}

    }
	
	@CrossOrigin
	@RequestMapping(value="/index",method=RequestMethod.POST)
    public File index(@RequestParam(required = true, value = "file") MultipartFile file, @RequestParam(required = false, value = "userid", defaultValue="1") Long userId,
            @RequestParam(required = false, value = "chunks", defaultValue="1") Integer chunks, @RequestParam(required = false, value = "chunk", defaultValue="1") Integer chunk) {  
		File result = this.storageService.store(file,userId, chunk, chunks);
		if(result.getStatus() == Status.FINISHED) {
			files.add(result);
		}
        return result;
    }
}