package br.com.wcdoliveira.hotchallenge.filegateway.application;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import br.com.wcdoliveira.hotchallenge.filegateway.storage.StorageProperties;
import br.com.wcdoliveira.hotchallenge.filegateway.storage.StorageService;

@SpringBootApplication
@ComponentScan({"br.com.wcdoliveira.hotchallenge.filegateway.controller","br.com.wcdoliveira.hotchallenge.filegateway.model","br.com.wcdoliveira.hotchallenge.filegateway.storage"})
@EnableConfigurationProperties(StorageProperties.class)
public class HotChallengeFileGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotChallengeFileGatewayApplication.class, args);
	}
	
    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> { 
            storageService.deleteAll();
            storageService.init();
        };
    }
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}