package br.com.wcdoliveira.hotchallenge.filegateway.storage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import br.com.wcdoliveira.hotchallenge.filegateway.model.File;
import br.com.wcdoliveira.hotchallenge.filegateway.model.Status;

@Service
public class FileSystemStorageService implements StorageService {

	@Value("${path.get_file_uri}")
	private String publicBaseLink;

	private final Path rootLocation;

	@Autowired
	public FileSystemStorageService(StorageProperties properties) {
		this.rootLocation = Paths.get(properties.getLocation());
	}

	@Override
	public File store(MultipartFile file, Long userId, int chunk, int chunks) {
		File result;
		Status statusToSet = Status.PROCESSING;
		try {
			String fileName = StringUtils.cleanPath(file.getOriginalFilename());
			if (chunk == chunks) {
				Files.write(this.rootLocation.resolve(fileName), file.getBytes(), StandardOpenOption.CREATE);
				statusToSet = Status.FINISHED;
			} else {
				Files.write(rootLocation, file.getBytes(), StandardOpenOption.APPEND);
				statusToSet = Status.PROCESSING;
			}
			result = new File(userId, fileName, statusToSet, new Date(), chunks,
					"http://localhost:8081/" + this.rootLocation.resolve(fileName).toString());
		} catch (Exception ignored) {
			ignored.printStackTrace();
			result = new File(userId, StringUtils.cleanPath(file.getOriginalFilename()), Status.ERROR, new Date(), 0,
					"");
		}
		return result;
	}

	@Override
	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void init() {
		try {
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}
}
