
package br.com.wcdoliveira.hotchallenge.filegateway.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import br.com.wcdoliveira.hotchallenge.filegateway.application.HotChallengeFileGatewayApplication;
import br.com.wcdoliveira.hotchallenge.filegateway.controller.FileController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FileController.class)
@ContextConfiguration(classes = {HotChallengeFileGatewayApplication.class})
public class FileControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void indexGET() throws Exception {
		this.mockMvc.perform(get("/index")
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));		
		this.mockMvc.perform(post("/"))
				.andExpect(status().isNotFound());
	}
	


	@Test
	public void indexPOST() throws Exception{
		MockMultipartFile mockFile = new MockMultipartFile("file", "file.txt", "text/plain", "file".getBytes());
		this.mockMvc.perform(multipart("/index")
				.file(mockFile))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));
	}
	
	@Test
	public void uploadGET() throws Exception {
		this.mockMvc.perform(get("/upload-dir/file")
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));		
	}
	
	@Test(expected = NestedServletException.class)
	public void testNestedServletException() throws Exception {
		this.mockMvc.perform(post("/index"));
	}
}