package br.com.wcdoliveira.hotchallenge.web.application;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"br.com.wcdoliveira.hotchallenge.web.controller","br.com.wcdoliveira.hotchallenge.web.model"})
public class HotChallengeWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotChallengeWebApplication.class, args);
	}
}
