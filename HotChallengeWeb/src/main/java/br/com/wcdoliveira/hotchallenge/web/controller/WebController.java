package br.com.wcdoliveira.hotchallenge.web.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import br.com.wcdoliveira.hotchallenge.web.model.File;

@Controller
public class WebController {
	
	@Value("${path.list_files_uri}")
	private String LIST_FILES_URI;
	
	@Value("${path.post_files_uri}")
	private String POST_FILE_URI;
		
	@RequestMapping("/index")
    public String index() {
        return "index";
    }
	
	@RequestMapping("/listararquivos")
    public String listarArquivos(Model model) {
	    RestTemplate restTemplate = new RestTemplate();
	    try {
		    ResponseEntity<List<File>> response = restTemplate.exchange(
		    		LIST_FILES_URI, HttpMethod.GET, null, new ParameterizedTypeReference<List<File>>() {}
		    );
		    List<File> files = response.getBody();  
		    if(files.size() > 0) {
			    model.addAttribute("files", files);		    	
		    }else {
				model.addAttribute("message", "Nenhum registro encontrado. Tente novamente.");
		    }
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "Falha ao buscar registros. Tente novamente.");
		}
        return "listar_arquivos";
    }
	
	@RequestMapping("/enviararquivo")
    public String enviarArquivo(Model model) {
	    model.addAttribute("action", POST_FILE_URI);
        return "enviar_arquivo";
    }
}